# challenge-front

Front of challenge-developer

## Iniciando

Las siguientes instrucciones son para descargar y desplegar el front del aplicativo challenge

### Instalacion


Descargar el repositorio 

```
git clone https://gitlab.com/AlejoE02/challenge-front.git
```

entrar a la carpeta que se descargó

```
cd challenge-front
```

ejecutar el comando de instalacion de dependencias

```
npm install
```

Por ultimo inicializar el servidor local del front

```
ng serve --open
```

Desde el navegador web entrar a la url del localhost

```
http://localhost:4200/
```
