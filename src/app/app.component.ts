import { Component, SimpleChanges } from '@angular/core';

import { FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient, HttpHeaders } from "@angular/common/http";


export interface TableFound {
  id: number;
  email: string;
  location: string;
  area_m2: number;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  email: any;
  location: any;
  area_m2: any;
  errorMessage: any;
  locationid: any;
  title = 'challenge-front';
  locationFormControl = new FormControl('', [
    // Validators.required,
    // Validators.email,

  ]);
  dataSource = new MatTableDataSource<TableFound>();

  tableConsult: TableFound[] = [];
  displayedColumns: string[] = ['id', 'email', 'location', 'area_m2'];

  constructor(private http: HttpClient) {
    this.dataSource = new MatTableDataSource(this.tableConsult);
  }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.getLocations();
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.getLocations();
  }

  getLocations() {
    const arrayResponse: any = [];
    this.http.request('GET', 'http://localhost:3000/api/getAll', { responseType: "json" }).subscribe(
      result => {
        let arreglo = Object.entries(result)
        arreglo.forEach(function callback(current, index, value) {
          for (let i = 0; i <= current.length; i++) {
            // console.log('Value', value[index][1][i]);
            if (value[index][1][i]) {
              console.log('entro')
              console.log('Value', value[index][1][i]);
              arrayResponse.push({
                id: value[index][1][i].id,
                email: value[index][1][i].email,
                location: value[index][1][i].location,
                area_m2: value[index][1][i].area_m2,
              })
            }
          }
        });
        this.tableConsult = arrayResponse;
        this.dataSource.data = this.tableConsult;
        // console.log('Data', this.dataSource.data)
        // console.log('Array Final',arrayResponse);
        // console.log('Table Consult',this.tableConsult);
      }
    )
  };

  saveLocation() {
    const headers = new HttpHeaders().set("Content-Type", "application/json")
    this.http.post('http://localhost:3000/api/register', {
      "email": this.email,
      "location": this.location,
      "area_m2": this.area_m2
    }).subscribe(
      (val) => {
        console.log("POST call successful value returned in body",
          val);
      },
      response => {
        if (response.status !== '200') {
          this.errorMessage = response.error.message;
          console.log("POST call in error", response);
        }
      },
      () => {
        console.log("The POST observable is now completed.");
        this.getLocations();
      }
    );
  }

  deleteLocation() {
    this.http.delete('http://localhost:3000/api/location/' + this.locationid).subscribe(
      (val) => {
        console.log("DELETE call successful value returned in body",
          val);
      },
      response => {
        console.log("DELETE call in error", response);
      },
      () => {
        console.log("The DELETE observable is now completed.");
        this.getLocations();
      });
  }
}
